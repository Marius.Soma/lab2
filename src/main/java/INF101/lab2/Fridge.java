package INF101.lab2;

import java.util.List;
import java.util.ArrayList;


import edu.hm.hafner.util.NoSuchElementException;

public class Fridge implements IFridge {
    int max_size = 20;

    public List<FridgeItem> fridgeList = new ArrayList<>();

    public int totalSize(){
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return fridgeList.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridgeList.size() == 20) {
            return false;
        }else{
            fridgeList.add(item);
            return true;
        }   
    }   

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeList.contains(item)){
            fridgeList.remove(item);
        }else{
        throw new NoSuchElementException("Could not find " + item);
        }
    }

    @Override
    public void emptyFridge() {
        fridgeList.removeAll(fridgeList);
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> itemsRemoved = new ArrayList<>();
        for(FridgeItem itemInList : fridgeList){
            if(itemInList.hasExpired()){
                itemsRemoved.add(itemInList);
            }
        }
        for (FridgeItem removingItems : itemsRemoved){
            fridgeList.remove(removingItems);
        }

        return itemsRemoved;
    }
}
